'use strict'
module.exports = {
  NODE_ENV: '"production"',
  HOST_BASE_URL: JSON.stringify(process.env.HOST_BASE_URL || 'https://api.dantser.net/x/account/v1')
}
