'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  HOST_BASE_URL: JSON.stringify(process.env.HOST_BASE_URL || 'https://api-dev.dantser.net/x/account/v1')
})
